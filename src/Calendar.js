import moment from "moment";

export class Calendar {
    constructor() {
        this.date = moment().date();
        this.day = moment().format("MMMM YYYY");
        this.daysinMonth = moment().daysInMonth();
        this.element = false;
    }
    /**
     * Méthode qui intialise le mois en cours
     * @param {HTMLElement} element 
     */
    drawMonths(element) {
        element.textContent = this.day;
    }

    /**
     * Méthode qui dessine un tableau qui correspond au calendrier
     */
    drawGrid() {
        if (!this.element) {
            let table = document.createElement("table");
            table.setAttribute("id", "calendarTable")
            this.element = table;
        }
        this.element.innerHTML = '';
        let tbody = document.createElement("tbody");
        tbody.setAttribute("id", "calendarTable")
        this.element.setAttribute("id", "calendarTable")
        for (let index = 0; index < 37; index++) {
            let tr = document.createElement("tr");
            let td = document.createElement('td');
            tr.appendChild(td);
            tr.classList.add('grid');
            tbody.appendChild(tr);
        }
        this.element.appendChild(tbody);
        return this.element
    }
}

