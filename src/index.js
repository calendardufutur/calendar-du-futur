//IMPORT //
import moment from 'moment';
import { Calendar } from './Calendar';
import { Events } from './Events';
import './scss/style.scss';

//HTML QUERY //
let grid = document.querySelector('#grid');
let months = document.querySelector('#month');
let prev_arrow = document.querySelector('#previous_arrow');
let next_arrow = document.querySelector('#next_arrow');
let nameEvent = document.querySelector('#name-event');
let placeEvent = document.querySelector('#place-event');
let dateEvent = document.querySelector('#date-event')
let button_submit = document.querySelector('#btn-submit');
let btnDel = document.querySelector('#btn-delete');
let eventForm = document.querySelector('#event-form');
let eventList = document.querySelector('#event-list');
let newEventTitle = document.querySelector('#newEventTitle');
let category_list = document.querySelector("#category-list")

//INSTANCE OF CALENDAR //
let eventsTable = [];
let calendar = new Calendar;

//DRAWING CALENDAR //
calendar.drawMonths(months);
grid.appendChild(calendar.drawGrid());
let caseSelector = document.querySelectorAll('.grid')
monthNumbers(caseSelector);
eventForm.style.display = 'none';
createEvents(caseSelector);
drawEvents(caseSelector);


//PREVIOUS ARROW EVENT LISTENER //
prev_arrow.addEventListener('click', () => {
    let prevMonth = moment(months.textContent, 'MMMM YYYY').subtract(1, 'months');
    months.textContent = moment(prevMonth).format('MMMM YYYY');
    calendar.drawGrid();
    let caseSelector_prev = document.querySelectorAll('.grid');
    monthNumbers(caseSelector_prev);
    eventForm.style.display = 'none';
    createEvents(caseSelector_prev);
    drawEvents(caseSelector_prev);
});

//NEXT ARROW EVENT LISTENER //
next_arrow.addEventListener('click', () => {
    let nextMonth = moment(months.textContent, 'MMMM YYYY').add(1, 'months');
    months.textContent = moment(nextMonth).format('MMMM YYYY');
    calendar.drawGrid();
    let caseSelector_next = document.querySelectorAll('.grid');
    monthNumbers(caseSelector_next);
    eventForm.style.display = 'none';
    createEvents(caseSelector_next);
    drawEvents(caseSelector_next);
});

//USEFUL FUNCTIONS//
/**
 * Fonction qui crée des instance d'Events et les met dans le tableau eventsTable[]
 * @param {HTMLElement} selector tableau contenant les élements HTML du calendrier 
 */
function createEvents(selector) {
    for (let days = 0; days < selector.length; days++) {
        const element = selector[days];
        element.addEventListener('click', () => {
            element.classList.add('selector');
            dateEvent.value = `${element.textContent} ${months.textContent}`;
            button_submit.addEventListener('click', () => {
                if (element.classList.contains('selector')) {
                    let newEvent = new Events(nameEvent.value, element.textContent, months.textContent,
                        placeEvent.value, category_list.value)
                    newEvent.isThereEvent = true;
                    eventsTable.push(newEvent);
                    colorFilter(newEvent, element);
                    //Ajoute les nouveaux events dans List Event
                    if (newEvent) {
                        eventList.style.display = 'block';
                        let pList = document.createElement('p');
                        eventList.appendChild(pList);
                        pList.innerHTML = `${newEvent.name} le ${newEvent.day} ${newEvent.month} à ${newEvent.place}`;
                        
                    }
                }
                eventForm.style.display = 'none';
                element.classList.remove('selector');
            });
            drawForm(element);
        })

    }
}
//Button Delete
btnDel.addEventListener('click', function () {
    for (let index = 0; index < caseSelector.length; index++) {
        const days = caseSelector[index];
        for (let index = 0; index < eventsTable.length; index++) {
            const events = eventsTable[index];
            if (months.textContent === events.month && days.textContent === events.day) {
                days.style.backgroundColor = 'white';
                eventsTable[index].splice(index, 1);
            }
        }

    }
});
/**
 * Fonction qui dessine les Events sur le calendrier
 * @param {HTMLElement} selector tableau contenant les élements HTML du calendrier
 *//**
* Fonction qui permet de modifier un event
* @param {HTMLElement} selector tableau contenant les élements HTML du calendrier 
*/
function drawEvents(selector) {
    for (let index = 0; index < selector.length; index++) {
        const days = selector[index];
        for (let index = 0; index < eventsTable.length; index++) {
            const events = eventsTable[index];
            if (months.textContent === events.month && days.textContent === events.day) {
                colorFilter(events, days);
            }

        }
    }
}

/**
 * Fonction qui affiche les jours du mois
 * @param {HTMLElement} caseSelector tableau contenant les élements HTML du calendrier
 */
function monthNumbers(caseSelector) {
    for (let selector = 0; selector < caseSelector.length; selector++) {
        for (let daysInMonth = 0; daysInMonth < moment(months.textContent, 'MMMM YYYY').daysInMonth(); daysInMonth++) {
            caseSelector[daysInMonth + moment(months.textContent, 'MMMM YYYY').startOf('month').day()].textContent = daysInMonth + 1;
            today(daysInMonth);
        }
    }
    /**
    * Fonction qui ajoute une couleur au jour actuel
    * @param {HTMLElement} selector tableau contenant les élements HTML du calendrier
    */
    function today(selector) {
        if (months.textContent === calendar.day &&
            parseInt(caseSelector[selector].textContent) === calendar.date) {
            caseSelector[selector].style.backgroundColor = 'pink';
        }
    }
}

/**
 * Fonction qui affiche le formulaire de création d'évènement si aucun event ce jour
 * @param {HTMLElement} selector tableau contenant les élements HTML du calendrier
 */
function drawForm(selector) {
    if (isThereEvent(selector)) {
        eventForm.style.display = 'block';
        eventList.style.display = 'none';
        newEventTitle.textContent = 'Modify Event';        
        for (let index = 0; index < eventsTable.length; index++) {
            const events = eventsTable[index];
            nameEvent.value = events.name;
            placeEvent.value = events.place;
            if (events.isThereEvent && events.day === selector.textContent && months.textContent === events.month) {
                let modifiedEvents = eventsTable.filter(() => {
                    eventsTable.splice(index, 1);
                    return eventsTable
                });
                eventsTable.push(modifiedEvents[index]);
                eventsTable.splice(eventsTable.length - 1, 1);
            }
        }
    } else {
        newEventTitle.textContent = 'New Event';
        eventList.style.display = 'none';
        eventForm.style.display = 'block';
    }

}


/**
 * Fonction qui affiche le créateur d'évènement
 * @param {HTMLElement} selector tableau contenant les élements HTML du calendrier
 */

function isThereEvent(selector) {
    for (let index = 0; index < eventsTable.length; index++) {
        const events = eventsTable[index];
        if (events.isThereEvent && events.day === selector.textContent && months.textContent === events.month) {
            return true
        }
    }
}
/**
 * Fonction qui assigne une couleur en fonction du type d'évènement choisi par l'utilisateur
 * @param {Events} event l'instance de la classe Events
 * @param {HTMLElement} element l'élement HTML à modifier
 */
function colorFilter(event, element) {
    if (event.category === 'Santé') {
        element.style.backgroundColor = 'green';
    } else if (event.category === 'Famille') {
        element.style.backgroundColor = 'orange';
    } else if (event.category === 'Professionnel') {
        element.style.backgroundColor = 'yellow';
    } else if (event.category === 'Personnel') {
        element.style.backgroundColor = 'red';
    } else if (event.category === 'Sortie') {
        element.style.backgroundColor = 'grey';
    }
}




