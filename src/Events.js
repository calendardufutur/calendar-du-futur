import { Calendar } from './Calendar'

export class Events {
    /**
     * 
     * @param {string} name Name of the event 
     * @param {number} date Date of the event
     * @param {string} place Place of the event
     */
    constructor(name, day, month, place, category) {
        this.name = name;
        this.day = day;
        this.month = month
        this.place = place;
        this.category = category
        this.isThereEvent = false;
    }
}

