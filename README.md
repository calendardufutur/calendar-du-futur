# Challenge : Calendrier JS

L'objectif de ce challenge sera de réaliser une petite application d'agenda par groupes de trois personnes en 2 semaines.

### Conception

- Réalisation d'un diagramme des Use Case en se basant sur les users stories ;
- Réalisation d'un diagramme de classe ;
- Réalisation de maquettes (wireframe);
- Réalisation de tests unitaires et fonctionnels;

### Technique

- Utilisation de Npm et Webpack ;
- Utilisation de la librairie moment.js pour la gestion des dates ( https://momentjs.com/ ) ;
- Création d'au moins une classe ;
- Application responsive (bootstrap) ;
- Documenter votre code : mettre la JS Doc sur toutes les fonctions et les méthodes pour dire ce qu'elles sont sensées faire ;
- Utilisation de git et gitlab pour le travail en groupe : créer les milestones, lister les tâches, assigner les tâches, travailler sur un dépôt commun.

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.

### User Stories

1. En tant qu'utilisateur.ice, je veux pouvoir **visualiser** tous mes événements du mois en cours afin de pouvoir m'organiser à court terme.
2. En tant qu'utilisateur.ice, je veux pouvoir **créer** de nouveaux événements afin de ne pas oublier de futurs rendez-vous.
3. En tant qu'utilisateur.ice, je veux pouvoir **modifier** les événements existants afin de pouvoir annuler ou reporter certains d'entre eux.
4. En tant qu'utilisateur.ice, je veux pouvoir **catégoriser** mes évènements afin de pouvoir facilement les identifier.

### Maquettes

##### Mobile-first

![Maquette mobile](Maquettes/Maquette mobile.png)

##### Wireframe

![Maquette PC](Maquettes/Maquette PC.png)

##### Use Case

Le user à la possibilité d'effectuer différentes actions sur le calendrier :

- **Changer de mois** en cliquant sur les flêches.
- **Créer un nouvel évènement** en cliquant sur le jour de son choix.
- **Choisir une couleur d'évènement**  dans le formulaire de création d'évènement.
- **Modifier un évènement** ou **supprimer un évènement** en cliquant sur un jour contenant un évènement.
- **Filtrer les évènements** grâce aux catégories prédéfinies.

![Diagramme Use Case](Maquettes/Diagramme Use Case.png)



##### Class Diagram

Le calendrier possède deux classes : 

- Calendar qui permet de dessiner le calendrier
- Events qui regroupe les propriétés des évènements crées.

![Diagramme Classe](Maquettes/Diagramme Classe.png)

### More

Author : @mosalah69

​			   @Maaxichou

​			   @Oriana.A

Date : 09/09/2019 - 24/09/2019

