/// <reference types="Cypress" />

describe('Event form page', () => {

    it('Should add event on Event List ', () => {
        cy.visit('http://localhost/calendar-du-futur');
        cy.get('#grid').contains('5').click();
        cy.get('#name-event').type('Resto');
        cy.get('#place-event').type('Lyon');
        cy.get('#category-list').select('Sortie');
        cy.get('#btn-submit').click();
        cy.get('p').contains("Resto le 5 September 2019 à Lyon");
    });

    it('Should add another event on Event List ', () => {
        cy.get('#grid').contains('15').click();
        cy.get('#name-event').clear();
        cy.get('#name-event').type('Kiné');
        cy.get('#category-list').select('Santé')
        cy.get('#place-event').clear();
        cy.get('#place-event').type('Villefranche');
        cy.get('#btn-submit').click();
        cy.get('p').contains("Resto le 5 September 2019 à Lyon");
        cy.get('p').contains("Kiné le 15 September 2019 à Villefranche");
    });

    it('Should add event on Event List from another month', () => {
        cy.get('#next_arrow').click();
        cy.get('#grid').contains('24').click();
        cy.get('#name-event').clear();
        cy.get('#name-event').type('Meetup');
        cy.get('#category-list').select('Professionnel')
        cy.get('#place-event').clear();
        cy.get('#place-event').type('Ninkasi');
        cy.get('#btn-submit').click();
        cy.get('p').contains("Resto le 5 September 2019 à Lyon");
        cy.get('p').contains("Kiné le 15 September 2019 à Villefranche");
        cy.get('p').contains("Meetup le 24 October 2019 à Ninkasi");
    });

});