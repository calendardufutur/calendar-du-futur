/// <reference types="Cypress" />
import { Calendar } from "../src/Calendar";
import  moment from "moment";

describe('Test class Calendar', () => {
    let calendar;
    beforeEach(() => {
        calendar = new Calendar;
        cy.visit('http://localhost/calendar-du-futur/');
    });
    /**
     * Test méthode drawGrid()
     */
    it('should draw a grid', () => {
        cy.get('#calendarTable').should('have.length', 1);
        cy.get('.grid').should('have.length', 37);
    });
    /**
     * Test méthode drawMonths()
     */
    it('should show the actual month', () => {
        cy.get("#month").contains(moment().format("MMMM YYYY"))
    })
})