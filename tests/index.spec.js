/// <reference types="Cypress" />
import { Calendar } from "../src/Calendar";

describe('Test function in index', () => {
    let calendar;
    beforeEach(() => {
        calendar = new Calendar;
        cy.visit('http://localhost/calendar-du-futur/')
    })
    /**
     * Test fonction drawMonths()
     */
    it('should write the date in the correct case', () => {
        cy.get('.grid').should('include.text', '123456789101112131415161718192021222324252627282930')
    });
})